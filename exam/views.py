from django.template.context_processors import csrf

from django.core.urlresolvers import reverse_lazy

from django.contrib.auth.decorators import login_required

from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from django.contrib.auth import authenticate, login, logout

from datetime import datetime
from models import *


@require_POST
def signin(request):
    user = authenticate(username=request.POST['username'], password=request.POST['password'])
    if user is not None:
        login(request, user)
        return redirect('dashboard')
    else:
        return HttpResponse("Username and password doesnt match")


@login_required(login_url=reverse_lazy('login'))
def dashboard(request):
    user = request.user
    if hasattr(user, 'student'):
        score = 0
        context = {'user': user, 'tests': Test.objects.all()}
        return render(request, 'student/student.html', context)
    elif hasattr(user, 'faculty'):
        if user.faculty.isHOD:
            context = {'user': user, 'tests': Test.objects.all()}
            return render(request, 'hod/hod.html', context)
        else:
            context = {'user': user, 'tests': Test.objects.all()}
            return render(request, 'faculty/faculty.html', context)


@require_POST
@login_required(login_url=reverse_lazy('login'))
def edit_profile(request):
    user = request.user
    user.first_name = request.POST['first_name']
    user.last_name = request.POST['last_name']
    user.email = request.POST['email']
    user.save()
    if hasattr(user, 'student'):
        student = user.student
        print student.dob
        try:
            student.dob = request.POST['dob']
        except:
            print "Error updating DOB"
        student.gender = request.POST['gender']
        student.phone = request.POST['phone']
        
        ug_education = student.education.get()
        ug_education.college = request.POST['college']
        if request.POST['percentage']:
            ug_education.percentage = request.POST['percentage']
        if request.POST['start_year']:
            ug_education.start_year = request.POST['start_year']
        if request.POST['end_year']:
            ug_education.end_year = request.POST['end_year']
        ug_education.degree = request.POST['degree']
        ug_education.save()
        
        student.save()
        print student.dob
        
    elif hasattr(user, 'faculty'):
        faculty = user.faculty
        if faculty.isHOD:
            pass
        else:
            pass
    return redirect('profile')
    

@require_POST
def settings(request):
    pass


@login_required(login_url=reverse_lazy('login'))
def log_out(request):
    logout(request)
    return redirect('login')


def login_view(request):
    context = {}
    context.update(csrf(request))
    return render(request, 'login.html')


def settings_view(request):
    context = {}
    context.update(csrf(request))
    pass


@login_required(login_url=reverse_lazy('login'))
def profile_view(request):
    user = request.user
    if hasattr(user, 'student'):
        ug_education = user.student.education.get()
        context = {'user': user, 'student': user.student, 'education': ug_education}
        context.update(csrf(request))
        return render(request, 'student/profile.html', context)


@require_POST
@login_required(login_url=reverse_lazy('login'))
def change_password(request):
    user = authenticate(username=request.user.username, password=request.POST['old_pass'])
    if user:
        user.set_password(request.POST['new_pass'])
    else:
        return HttpResponse("Error: Old and New Passwords dont match")
    user.save()
    return redirect('login')


def change_passwd_view(request):
    context = {}
    context.update(csrf(request))
    return render(request, 'change_passwd.html', context)


@require_POST
@login_required(login_url=reverse_lazy('login'))
def create_test(request):
    user = request.user
    if hasattr(user, 'faculty') and user.faculty.isHOD:
        test = Test(
            year=request.POST['year'],
            weightage=request.POST['weightage'],
            neg_mark=request.POST['neg_marks'],
            pos_mark=request.POST['pos_marks'],
            duration=request.POST['duration'],
            time=request.POST['time'],
            date=request.POST['date']
        )
        test.save()
    return redirect('dashboard')


def view_tests(request):
    context = {}
    return redirect('dashboard')
    # return render(request, '', context)


@login_required(login_url=reverse_lazy('login'))
def create_question(request, round_no):
    user = request.user
    if hasattr(user, 'faculty'):
        test = get_object_or_404(Test, id=round_no)
        topics = Topic.objects.all()
        questions_list = []
        for topic in topics:
            questions = Question.objects.filter(test=test, topic=topic)
            if questions:
                questions_list.append(questions)
    
        context = {'test': test, 'questions_list': questions_list, 'topics': topics, 'hod': user.faculty.isHOD}
        context.update(csrf(request))
        return render(request, 'faculty/add_question.html', context)


@login_required(login_url=reverse_lazy('login'))
def delete_question(request, round_no, qtn_id):
    question = Question.objects.get(id=qtn_id)
    question.delete()
    return redirect('new_question', round_no)


@require_POST
@login_required(login_url=reverse_lazy('login'))
def create_qsn_view(request):
    user = request.user
    if hasattr(user, 'faculty'):
        faculty = user.faculty
        if faculty.isHOD:
            pass
        else:
            if 'add_next' in request.POST:
                topic = Topic.objects.get(topic=request.POST['topic'])
                test_id = request.POST['id']
                test = Test.objects.get(id=test_id)
                question = Question(question=request.POST['question'], topic=topic, test=test, faculty=faculty)
                question.save()
                rb_value = request.POST['rb']

                choice1 = Choice(choice=request.POST['opt1'], question=question)
                choice2 = Choice(choice=request.POST['opt2'], question=question)
                choice3 = Choice(choice=request.POST['opt3'], question=question)
                choice4 = Choice(choice=request.POST['opt4'], question=question)
                if rb_value == '1':
                    choice1.is_correct = True
                elif rb_value == '2':
                    choice2.is_correct = True
                elif rb_value == '3':
                    choice3.is_correct = True
                elif rb_value == '4':
                    choice4.is_correct = True
                choice1.save()
                choice2.save()
                choice3.save()
                choice4.save()

                return redirect('new_question', test_id)
            else:
                return redirect('dashboard')


@login_required(login_url=reverse_lazy('login'))
def approve_test(request, round_no):
    user = request.user
    if hasattr(user, 'faculty') and user.faculty.isHOD:
        # Get question object
        test = get_object_or_404(Test, id=round_no)

        context = {'test': test, 'questions': Question.objects.filter(test=test)}
        context.update(csrf(request))
        return render(request, 'hod/approve_test.html', context)


@login_required(login_url=reverse_lazy('login'))
def submit_test(request):
    return redirect('dashboard')

@require_POST
@login_required(login_url=reverse_lazy('login'))
def save_test(request):
    user = request.user
    if hasattr(user, 'faculty') and user.faculty.isHOD:
        if 'aprv_qtn' in request.POST:
            checkbox_values = request.POST.getlist('checks')
            for i, val in enumerate(checkbox_values):
                qtn = Question.objects.get(id=val)
                qtn.is_approved = True
                qtn.save()
        elif 'aprv_test' in request.POST:
            testid = request.POST['testid']
            test = Test.objects.get(id=testid)
            test.is_approved = True
            test.save()
        return redirect('dashboard')

@login_required(login_url=reverse_lazy('login'))
def hod_notif(request):
    if hasattr(request.user, 'faculty'):
        tests = Test.objects.all()
        context = {'tests': tests}
        return render(request, 'faculty/notification.html', context)


@login_required(login_url=reverse_lazy('login'))
def view_ifapproved(request, round_no):
    if hasattr(request.user, 'faculty'):
        test = Test.objects.get(id=round_no)
        questions = Question.objects.filter(test=test)
        context = {'test': test, 'questions': questions}
        return render(request, 'faculty/view_ifapproved.html', context)


@login_required(login_url=reverse_lazy('login'))
def test_instructions(request, round_no):
    if hasattr(request.user, 'student'):
        test = Test.objects.get(id=round_no)
        context = {'test': test}
        return render(request, 'student/instructions.html', context)


@login_required(login_url=reverse_lazy('login'))
def test_start(request, round_no):
    user = request.user
    if hasattr(user, 'student'):
        flag = 0
        test = Test.objects.get(id=round_no)
        questions = Question.objects.filter(test=test, is_approved=True)
        
        if not QuestionsOrder.objects.filter(test=test):
            TestTaken(test=test, student=user.student, start_time=datetime.now()).save()
            for i, question in enumerate(questions):
                QuestionsOrder(question=question, test=test, order=i).save()
            flag = 1
        
        questions = QuestionsOrder.objects.filter(test=test).order_by('order')
        for i, question in enumerate(questions):
            if question.flag == 0:  # If it is not Flagged or Submitted
                question_id = i + 1
                break
        else:
            question_id = 0
        
        if flag is 1:
            question_id = 1
        
        context = {'test': test, 'questions': questions, 'question_id': question_id}
        return render(request, 'student/test.html', context)


@require_POST
@login_required(login_url=reverse_lazy('login'))
def submit_question(request):
    user = request.user
    if hasattr(user, 'student'):
        flag = int(request.POST['flag'])
        question_id = request.POST['question_id']
        question = Question.objects.get(id=question_id)
        choice = Choice.objects.get(id=request.POST['choice_'+str(question_id)])
        test = Test.objects.get(question=question)
        question_order = question.question_order.get()
        if flag == 0:   # Submit
            question_order.flag = 2
            test_taken = TestTaken.objects.filter(test=test, student=user.student)[0]   # Dont allow Multiple testTaken
            if choice.is_correct:
                test_taken.right += 1
            else:
                test_taken.wrong += 1
            test_taken.save()
        else:   # Flag
            question_order.flag = 1
            question_order.flagged_choice = choice
        question_order.save()
        question.save()
    
        return redirect('test_start', test.id)
