from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime


class Student(models.Model):
    GENDER = (('M', 'Male'), ('F', 'Female'), ('O', 'Others'))
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='student')
    dob = models.DateField(null=True, auto_now=True)
    selected = models.IntegerField(default=0, blank=True, null=True)
    gender = models.CharField(max_length=10, choices=GENDER, default='M')
    year = models.IntegerField(default=datetime.now().year, blank=True, null=True)
    phone = models.IntegerField(default=None, null=True)

    def __str__(self):
        return str(self.user.first_name)


class Education(models.Model):
    college = models.CharField(max_length=200)
    percentage = models.IntegerField(default=0)
    start_year = models.IntegerField(null=True)
    end_year = models.IntegerField(null=True)
    degree = models.CharField(max_length=20)
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='education')

    def __str__(self):
        return str(self.college) + " " + str(self.percentage)


class Topic(models.Model):
    topic = models.CharField(max_length=200)

    def __str__(self):
        return str(self.topic)


class Faculty(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='faculty')
    isHOD = models.BooleanField(default=False)
    area_of_interest = models.CharField(max_length=100, default=None, null=True)
    authorized_to_set = models.CharField(max_length=100, default=None, null=True)

    def __str__(self):
        return str(self.user.first_name)


class Test(models.Model):
    year = models.IntegerField()
    weightage = models.IntegerField()
    round = models.IntegerField(default=1)
    neg_mark = models.FloatField(default=0.25)
    pos_mark = models.FloatField(default=1)
    duration = models.IntegerField()
    date = models.DateField()
    time = models.TimeField()
    is_approved = models.BooleanField(default=False)

    @property
    def max_marks(self):
        return self.pos_mark * self.question.count()
        
    def __str__(self):
        return str(self.year) + str(self.round)


class TestTaken(models.Model):
    start_time = models.TimeField(default=datetime.now())
    end_time = models.TimeField(default=None, null=True)
    wrong = models.IntegerField(default=0)
    right = models.IntegerField(default=0)
    logfile = models.FileField(default=None)
    status = models.CharField(max_length=100)
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='test_taken')
    test = models.ForeignKey(Test, on_delete=models.CASCADE, related_name='test_taken')

    @property
    def score(self):
        test = self.test
        return self.right * test.pos_mark - self.wrong * test.neg_mark
    
    @property
    def attempted(self):
        return self.right + self.wrong

    @property
    def not_attempted(self):
        return self.test.question.count() - self.attempted
    
    def __str__(self):
        return str(self.student) + " " + str(self.start_time)


class Question(models.Model):
    question = models.CharField(max_length=200)
    test = models.ForeignKey(Test, on_delete=models.CASCADE, related_name='question')
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, related_name='question')
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE, related_name='question')
    is_approved = models.BooleanField(default=False)
    
    def __str__(self):
        return str(self.question)


class Choice(models.Model):
    choice = models.CharField(max_length=100)
    is_correct = models.IntegerField(default=False)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='choice')

    def __str__(self):
        return str(self.choice)


class QuestionsOrder(models.Model):
    test = models.ForeignKey(Test, on_delete=models.CASCADE, related_name='question_order')
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='question_order')
    order = models.IntegerField(null=True)
    flagged_choice = models.ForeignKey(Choice, on_delete=models.CASCADE, related_name='question_order', default=None, null=True)
    flag = models.IntegerField(default=0) # 1=Flag, 2=Submit

    def __str__(self):
        return str(self.id) + " " + str(self.question)
