from django.contrib import admin

# Register your models here.
from exam.models import *

admin.site.register(Student)
admin.site.register(Faculty)

admin.site.register(Education)
admin.site.register(TestTaken)
admin.site.register(Test)
admin.site.register(Question)
admin.site.register(Choice)
admin.site.register(Topic)
