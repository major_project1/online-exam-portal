function goFullscreen() {
    // Must be called as a result of user interaction to work
    mf = document.getElementById("main_frame");
    mf.webkitRequestFullscreen();
    mf.style.display="";
}

function fullscreenChanged() {
    if (document.webkitFullscreenElement == null) {
    mf = document.getElementById("main_frame");
    mf.style.display="none";
    }
}
document.onwebkitfullscreenchange = fullscreenChanged;
document.documentElement.onclick = goFullscreen;
document.onkeydown = goFullscreen;
