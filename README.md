# README #

This is a basic Python django web application to host MCQ tests.


## Quick summary ##
* Python django and MySQL backend, HTML/CSS/Materialize frontend
* Implemented role based authorization for the roles of Student, Faculty and HoD
* Used Django CSRF middleware for security

### Student module: ###
* Entering the student profile information and Education details
* Timer for tests; students are not allowed to take test before the preset time
* Randomized order of questions which is different for each student
* Mark questions for review, which is stored on the server side
* Snapshot of submitted answer is stored on the server as images for proof of submission
* Scoring of tests and mailing students

### Faculty module: ###
* Add questions into the question pool for a particular test on different areas
* Associated a question with difficulty tag so that it becomes easier for the HoD to review and finalize questions
* View all the questions in the pool grouped by the area, for a particular test
* Set positive and negative weightage for questions in a test
* View performance of students in a test

### HoD module: ###
* Schedule a test
* Approve questions for a test and finalize the questions set

### How do I get set up? ###
* Configuration: Refer django docs at https://docs.djangoproject.com/en/2.1/releases/1.10/
* Dependencies: Check requirements.txt
* Database configuration:  To generate sample DB entries, run the contents of DB_Setup.txt on the Django Shell

### Who do I talk to? ###

* Repo owner or contributors