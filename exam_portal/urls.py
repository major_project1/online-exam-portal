"""exam_portal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from exam.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', dashboard),
    url(r'^login/?$', login_view, name='login'),
    url(r'^dashboard$', dashboard, name='dashboard'),
    url(r'^profile$', profile_view, name='profile'),
    url(r'^change_password$', change_passwd_view, name='change_password'),
    url(r'^settings$', settings_view, name='settings'),
    url(r'^logout$', log_out, name='logout'),
    url(r'^view_tests$', view_tests, name='view_tests'),
    url(r'^create_question$', create_qsn_view, name='create_question'),
    url(r'^approve_test/(?P<round_no>[0-9]+)/$', approve_test, name='approve_test'),
    url(r'^submit_test$', submit_test, name='submit_test'),
    url(r'^save_test$', save_test, name='save_test'),
    url(r'^hod_notif$', hod_notif, name='hod_notif'),
    url(r'^hod_notif/(?P<round_no>[0-9]+)/$', view_ifapproved, name='view_ifapproved'),
    url(r'^test_instructions/(?P<round_no>[0-9]+)/$', test_instructions, name='test_instructions'),
    url(r'^test_start/(?P<round_no>[0-9]+)/$', test_start, name='test_start'),
    url(r'^delete_question/(?P<round_no>[0-9]+)/(?P<qtn_id>[0-9]+)/$', delete_question, name='delete_question'),
    
    # Form handling
    url(r'^signin$', signin, name='signin'),
    url(r'^change_settings$', settings, name='change_settings'),
    url(r'^passwd$', change_password, name='passwd'),
    url(r'^edit_profile', edit_profile, name='edit_profile'),
    url(r'^test$', create_test, name='test'),
    url(r'^test/(?P<round_no>[0-9]+)/$', create_question, name='new_question'),
    url(r'^question$', create_qsn_view, name='view_question'),
    url(r'^submit_qsn', submit_question, name='submit_question'),

    # url('^', include('django.contrib.auth.urls')),
]
