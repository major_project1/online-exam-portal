# Run the contents on Django Shell

from django.contrib.auth.models import User
from exam.models import *
from datetime import datetime as dt

user = User.objects.create_user(
    email='student@student.com',
    username='student',
    password='student',
    first_name='student')
user.save()
student = Student(
    user=user,
    )
student.save()

user = User.objects.create_user(
    email='faculty@faculty.com',
    username='faculty',
    password='faculty',
    first_name='Faculty')
user.save()
faculty = Faculty(
    user=user
    )
faculty.save()

user = User.objects.create_user(
    email='hod@hod.com',
    username='hod',
    password='hod',
    first_name='Hod')
user.save()
hod = Faculty(
    user=user,
    isHOD=True
    )
hod.save()

ug_education = Education(student=student)
ug_education.save()

topic = Topic(topic='DSA')
topic.save()

topic = Topic(topic='WTA')
topic.save()

topic = Topic(topic='CCN')
topic.save()

topic = Topic(topic='DBMS')
topic.save()

test = Test(year=2017, weightage=100, round=1, duration=30, date=dt.date(dt.now()), time=dt.time(dt.now()))
test.save()

//Questions list
question = Question(question="Binary search tree has best case run-time complexity of ?(log n). What could the worst case?", test=test, topic=topic, faculty=faculty)
question.save()
Choice(choice="O(n)", is_correct=True, question=question).save()
Choice(choice="O(n2)", question=question).save()
Choice(choice="O(n3)", question=question).save()
Choice(choice="O(nlogn)", question=question).save()

question = Question(question="Which method can find if two vertices x & y have path between them?", test=test, topic=topic, faculty=faculty)
question.save()
Choice(choice="DFS", question=question).save()
Choice(choice="BFS", question=question).save()
Choice(choice="Both", is_correct=True, question=question).save()
Choice(choice="None", question=question).save()

question = Question(question="Which of the following summation operations is performed on the bits to check an error-detecting code?", test=test, topic=topic, faculty=faculty)
question.save()
Choice(choice="Checksum", is_correct=True, question=question).save()
Choice(choice="Attenuation", question=question).save()
Choice(choice="Coder-decoder", question=question).save()
Choice(choice="Codec", question=question).save()

question = Question(question="An error-detecting code inserted as a field in a block of data to be transmitted is known as:", test=test, topic=topic, faculty=faculty)
question.save()
Choice(choice="flow control", question=question).save()
Choice(choice="Checksum", question=question).save()
Choice(choice="Codec", question=question).save()
Choice(choice="Frame check sequence", is_correct=True, question=question).save()

question = Question(question="Primitive operations common to all record management systems include:", test=test, topic=topic, faculty=faculty)
question.save()
Choice(choice="Print", question=question).save()
Choice(choice="Look up", is_correct=True, question=question).save()
Choice(choice="Tree", question=question).save()
Choice(choice="None", question=question).save()

question = Question(question="Which of the following is not a logical data-base structure?", test=test, topic=topic, faculty=faculty)
question.save()
Choice(choice="Relation", question=question).save()
Choice(choice="Tree", is_correct=True, question=question).save()
Choice(choice="network", question=question).save()
Choice(choice="chain", question=question).save()

